use super::models::User;
use crate::db;
use rocket::request::FlashMessage;
use rocket_contrib::templates::Template;

#[derive(Debug, Serialize)]
struct UserContext<'a, 'b> {
    title: String,
    static_path: String,
    msg: Option<(&'a str, &'b str)>,
    users: Vec<User>,
}

impl<'a, 'b> UserContext<'a, 'b> {
    pub fn err(conn: &db::DbConn, msg: &'a str) -> UserContext<'static, 'a> {
        UserContext {
            msg: Some(("error", msg)),
            users: User::all(conn),
            title: String::from("Users list"),
            static_path: String::from("../"),
        }
    }

    pub fn raw(conn: &db::DbConn, msg: Option<(&'a str, &'b str)>) -> UserContext<'a, 'b> {
        UserContext {
            msg: msg,
            users: User::all(conn),
            title: String::from("Users list"),
            static_path: String::from("../"),
        }
    }
}

#[get("/users")]
pub fn show_users(msg: Option<FlashMessage>, conn: db::DbConn) -> Template {
    Template::render(
        "users",
        &match msg {
            Some(ref msg) => UserContext::raw(&conn, Some((msg.name(), msg.msg()))),
            None => UserContext::raw(&conn, None),
        },
    )
}

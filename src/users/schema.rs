table! {
    users (email) {
        email -> Varchar,
        username -> Varchar,
        password -> Text,
        role -> Varchar,
    }
}

use diesel::{self, prelude::*};

use super::schema::users;
use super::schema::users::dsl::users as all_users;

#[table_name = "users"]
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
pub struct User {
    pub email: String,
    pub username: String,
    pub password: String,
    pub role: String,
}

impl User {
    pub fn all(conn: &PgConnection) -> Vec<User> {
        all_users
            .order(users::email.desc())
            .load::<User>(conn)
            .unwrap()
    }
}

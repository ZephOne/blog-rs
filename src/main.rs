#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde_derive;

mod db;
mod users;

use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;

#[derive(Debug, Serialize)]
struct HomeContext {
    title: String,
    static_path: String,
}

impl HomeContext {
    pub fn home() -> HomeContext {
        HomeContext {
            title: String::from("Welcome"),
            static_path: String::from(""),
        }
    }
}

#[get("/")]
fn home() -> Template {
    Template::render("home", HomeContext::home())
}

fn main() {
    rocket::ignite()
        .attach(db::DbConn::fairing())
        .mount("/public", StaticFiles::from("static/"))
        .mount("/", routes![home])
        .mount("/", routes![users::router::show_users])
        .attach(Template::fairing())
        .launch();
}

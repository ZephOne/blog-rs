insert into users (email, username, password, role)
  values ('deb@ian.com', 'Red', '$2b$08$918zaVlrq5ukEv1q5ZWyteszg8vEBKcauySEXvrhJK/OBbAiapZXS', 'admin');
insert into users (email, username, password, role)
  values ('fed@ora.com', 'Blue', '$2b$08$NpJK3JG8G7qqlo/7q8JsHuadFKNX7ZBKMryTkDe4rDv6bDJqZz.06', 'user');
insert into users (email, username, password, role)
  values ('ar@ch.com', 'Black', '$2b$08$ERqMurM4rYvu.7jdp1crmu5DW5NBeLU.OEWfPlj.MsrUegOKsyrBC', 'user');
insert into users (email, username, password, role)
  values ('su@se.com', 'Green', '$2b$08$8qdQNIK3ykiUhrIvxLEBmuzxq3kuWeWSrIfs8wbcvl81XKXiNo02C', 'user');
insert into users (email, username, password, role)
  values ('ub@untu.com', 'Purple', '$2b$08$jWxR6cT4PVA76QMAyOrFEuPe1wofUEC.Ahmee3FnT1xR/fjyhkimu', 'user');

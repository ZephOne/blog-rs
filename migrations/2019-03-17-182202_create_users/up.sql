create table users (
  email varchar(100) PRIMARY KEY,
  username varchar(50) NOT NULL,
  password text NOT NULL,
  role varchar(50) NOT NULL
);
